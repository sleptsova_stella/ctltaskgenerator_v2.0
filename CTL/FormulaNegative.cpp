#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"
#include "Exceptions.h"
#include <iostream>

FormulaNegative::FormulaNegative(SubformulaCTL* subFormula) :SubformulaCTL()
{
	_p = subFormula;
	DepthSize = _p->DepthSize + 1;
};

std::set<State*>* FormulaNegative::SAT(KripkeStructure* kripkeStructure)
{
	auto resultStates = new std::set<State*>();

	for (auto stateAndPredicates : kripkeStructure->_marksFunction)
	{
		auto state = stateAndPredicates.first;
		auto predicates = stateAndPredicates.second;

		if (predicates.find(_p) == predicates.end())
			resultStates->insert(state);
	}

	return resultStates;
};

std::string FormulaNegative::GetFullFormula()
{
	std::string fullCTLFormula = "\\lnot (";
	if (_p != nullptr)
		return fullCTLFormula + _p->GetFullFormula() + ")";
	else
		throw new SubformulaIsEmpty(typeid(FormulaNegative).name());
};

std::string FormulaNegative::GetSubFormula()
{
	return "\\(f_{" + std::to_string(Number) + "} = \\lnot f_{" +
		std::to_string(_p->Number) + "},\\) " + _p->GetSubFormula();
};