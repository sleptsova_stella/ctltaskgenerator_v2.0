#pragma once
#include<string>

class Task;

#ifndef H_TaskInMathjaxFormat
#define H_TaskInMathjaxFormat

class TaskInMathjaxFormat
{
private:
	const char *_header = "<i>������ 3 </i> \n"
		"<p>����� \\( M = ( S, S_0, R, AP, L) \\) � ��������� ������, ���: </p> \n "
		" <ul style = \"list-style-type: none\"> \n ";
	const char *_startWording = "<p>���������, ����������� �� �� ������ "
		"��������� ������  ������� CTL: \\(";
	const char *_endWorking = "��������� ��������� �������� ������������ "
		"��� ������ \\ (\\ { \\mathrm{ E X }, \\mathrm{ A F }, \\mathrm{ E U } "
		"\\}\\) �� ������. </p> \n";
	const char * _subformulaDesignation = "<p> ��������� ���������� �������"
		" \\(\\varphi\\) ��������� �������: ";
	const char * _taskEnd = ".</p> \n <p>� ������� ��������� ��������� � ������ ���������, ���"
		"���������� �������� ��������� �������� ������.�������� ���������� �����.</p>";

	const std::string _sourceFileName = "CTL_Tasks.txt";

	Task* _task;
public: 
	TaskInMathjaxFormat(Task* task);
private: 
	void GenerateTaskInMathjaxFormat();
};

#endif