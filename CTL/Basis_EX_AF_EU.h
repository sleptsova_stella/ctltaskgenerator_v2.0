#pragma once
#include <string>
#include <set>

class __declspec(dllexport) KripkeStructure;


#ifndef H_State;
#define H_State

class __declspec(dllexport)  State
{
public:
	std::string Name;

public:
	State(std::string);
};
#endif

#ifndef H_SubformulaCTL;
#define H_SubformulaCTL
class __declspec(dllexport) SubformulaCTL
{
protected:
	static unsigned int InnerNumber;

	SubformulaCTL * _p = nullptr;
	SubformulaCTL* _q = nullptr;

public:
	int DepthSize;
	bool Marked = false;
	unsigned int Number = 0;

public:
	SubformulaCTL();
	virtual std::set<State*>* SAT(KripkeStructure*) = 0;
	virtual std::string GetFullFormula() = 0;
	virtual std::string GetSubFormula() = 0;
	
	unsigned int InitNumberFormula();

	friend class KripkeStructure;
};
#endif

#ifndef H_AtomicPredicate;
#define H_AtomicPredicate
class __declspec(dllexport) AtomicPredicate : public SubformulaCTL
{
private:
	std::string _name;
public:
	AtomicPredicate(std::string);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetName();
	std::string GetSubFormula();
};
#endif

#ifndef H_FormulaNegative;
#define H_FormulaNegative
class __declspec(dllexport) FormulaNegative : public SubformulaCTL
{
public:
	FormulaNegative(SubformulaCTL*);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetSubFormula();
};
#endif

#ifndef H_Or;
#define H_Or
class __declspec(dllexport) Or : public SubformulaCTL
{
public:
	Or(SubformulaCTL*, SubformulaCTL*);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetSubFormula();
};
#endif

#ifndef H_EX;
#define H_EX
class __declspec(dllexport) EX :public SubformulaCTL
{

public:
	EX(SubformulaCTL*);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetSubFormula();
};
#endif

#ifndef H_AF;
#define H_AF
class  __declspec(dllexport) AF : public SubformulaCTL
{
public:
	AF(SubformulaCTL*);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetSubFormula();
};
#endif


#ifndef H_EU;
#define H_EU
class __declspec(dllexport) EU : public SubformulaCTL
{
public:
	EU(SubformulaCTL*, SubformulaCTL*);
	std::set<State*>* SAT(KripkeStructure*);
	std::string GetFullFormula();
	std::string GetSubFormula();
};
#endif

bool CompareTwoStatesSets(std::set<State*> set1, std::set<State*> set2);