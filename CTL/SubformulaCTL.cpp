#include "stdafx.h"
#include "Basis_EX_AF_EU.h"

unsigned int SubformulaCTL::InnerNumber = 1;

SubformulaCTL::SubformulaCTL()
{
	InnerNumber = 1;
};

unsigned int SubformulaCTL::InitNumberFormula()
{
	auto num = 0;
	if (_p != nullptr && _p->Number == 0)
	{
		num = _p->InitNumberFormula() + 1;
		InnerNumber++;
	}

	if (_q != nullptr && _q->Number == 0)
	{
		Number = _q->InitNumberFormula() + 1;
		InnerNumber++;
	}
	else 
	{
		Number = num;
	}

	if (Number == 0)
	{
		Number = InnerNumber;
	}

	return InnerNumber;
};