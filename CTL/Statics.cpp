#include "stdafx.h"
#include "Basis_EX_AF_EU.h"

bool CompareTwoStatesSets(std::set<State*> set1, std::set<State*> set2)
{
	if (set1.size() != set2.size())
		return false;

	for (auto state1 : set1)
	{
		auto state1ExistsInSet2 = false;
		for (auto state2 : set2)
		{
			if (state1 == state2)
			{
				state1ExistsInSet2 = true;
				break;
			}
		}
		if (!state1ExistsInSet2)
			return false;
	}

	return true;
};
