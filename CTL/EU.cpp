#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"
#include "Exceptions.h"

EU::EU(SubformulaCTL* leftSubformula, SubformulaCTL* rightSubformula) :SubformulaCTL()
{
	_p = leftSubformula;
	_q = rightSubformula;
	DepthSize = max(_p->DepthSize, _q->DepthSize) + 1;
};

std::set<State*>* EU::SAT(KripkeStructure* kripkeStructure)
{
	auto X = kripkeStructure->_states;
	auto Y = new std::set<State*>();
	auto W = new std::set<State*>();

	for (auto stateAndPredicates : kripkeStructure->_marksFunction)
	{
		auto state = stateAndPredicates.first;
		auto predicates = stateAndPredicates.second;

		if (predicates.find(_p) != predicates.end())
			W->insert(state);

		if (predicates.find(_q) != predicates.end())
			Y->insert(state);
	}

	while (!CompareTwoStatesSets(X, *Y))
	{
		X = *Y;

		for (auto parentState : *W)
		{
			auto children = kripkeStructure->_statesWithChilren[parentState];
			for (auto child : children)
			{
				if (Y->find(child) != Y->end())
				{
					Y->insert(parentState);
					break;
				}
			}
		}
	}

	return Y;
};

std::string EU::GetFullFormula()
{
	std::string fullCTLFormula = "\\mathrm{E}( ";
	if (_p != nullptr)
		fullCTLFormula = fullCTLFormula + _p->GetFullFormula() + "\\mathrm{ U }";
	else
		throw new SubformulaIsEmpty(typeid(EU).name());

	if (_q != nullptr)
		return fullCTLFormula + _q->GetFullFormula() + ")";
	else
		throw new SubformulaIsEmpty(typeid(EU).name());
};

std::string EU::GetSubFormula()
{
	return "\\(f_{" + std::to_string(Number) +
		"} = \\mathrm{E}(f_{" + std::to_string(_p->Number) +
		"} \\mathrm{U} f_{" + std::to_string(_q->Number) + "}),\\) "
		+ _p->GetSubFormula() + _q->GetSubFormula();
};