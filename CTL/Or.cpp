#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"
#include "Exceptions.h"

Or::Or(SubformulaCTL* leftSubformula, SubformulaCTL* rightSubformula) :SubformulaCTL()
{
	_p = leftSubformula;
	_q = rightSubformula;
	DepthSize = max(_p->DepthSize, _q->DepthSize) + 1;
};

std::set<State*>* Or::SAT(KripkeStructure* kripkeStructure)
{
	auto resultStates = new std::set<State*>();

	for (auto stateAndPredicates : kripkeStructure->_marksFunction)
	{
		auto state = stateAndPredicates.first;
		auto predicates = stateAndPredicates.second;

		if (predicates.find(_p) != predicates.end() || predicates.find(_q) != predicates.end())
			resultStates->insert(state);
	}

	return resultStates;
};

std::string Or::GetFullFormula()
{
	std::string fullFormula = "(";
	if (_p != nullptr)
		fullFormula = fullFormula + _p->GetFullFormula() + " \\land ";
	else
		throw new SubformulaIsEmpty(typeid(EU).name());

	if (_q != nullptr)
		return fullFormula + _q->GetFullFormula() + ")";
	else
		throw new SubformulaIsEmpty(typeid(EU).name());
};


std::string Or::GetSubFormula()
{
	return "\\(f_{" + std::to_string(Number) +
		"} = f_{" + std::to_string(_p->Number) +
		"} \\land f_{" + std::to_string(_q->Number) + "}\\) "
		+ _p->GetSubFormula() + _q->GetSubFormula();
};