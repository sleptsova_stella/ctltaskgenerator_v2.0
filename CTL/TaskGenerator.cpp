#include "stdafx.h"
#include "TaskGenerator.h"
#include <stdlib.h>
#include <time.h>
#include "Exceptions.h"
#include "TaskInMathjaxFormat .h"

TaskGenerator::TaskGenerator(unsigned int tasksNum, unsigned int maxStatesNum,
	unsigned int maxEdgesNum, unsigned int maxPredicatesNum, unsigned int maxInitialStatesNum,
	unsigned int maxMarkedStatesNum)
{
	_tasksNum = tasksNum;
	_maxStatesNum = maxStatesNum;
	_maxEdgesNum = maxEdgesNum;
	_maxPredicatesNum = maxPredicatesNum;
	_maxInitialStatesNum = maxInitialStatesNum;
	_maxMarkedStatesNum = maxMarkedStatesNum;
};

void TaskGenerator::Generate()
{
	srand(time(NULL));

	for (unsigned int currentTask = 1; currentTask <= _tasksNum; currentTask++)
	{
		auto statesNum = GenerateStatesNum();
		auto edgesNum = GenerateEdgesNum();
		auto predicatesNum = GeneratePredicatesNum();
		auto initialStatesNum = GenerateInitailStatesNum();
		auto markedStatesNum = GenerateRandMarkedStatesNum();
		auto maxStatesNumInOneState = 3;

		auto task = new Task(statesNum, edgesNum, 3, initialStatesNum,
			markedStatesNum,maxStatesNumInOneState);

		auto taskInMathjaxFormat = new TaskInMathjaxFormat(task);
	}
};

unsigned int TaskGenerator::GenerateStatesNum()
{
	auto statesNum = _minStatesNum + (rand() % (_maxStatesNum - _minStatesNum));
	return statesNum;
};

unsigned int TaskGenerator::GenerateInitailStatesNum()
{
	auto initStatesNum = rand() % _maxInitialStatesNum;
	if (initStatesNum < _minInitialStatesNum)
		return initStatesNum;
};

unsigned int TaskGenerator::GenerateEdgesNum()
{
	auto edgesNum = rand() % _maxEdgesNum;
	if (edgesNum < _minEdgesNum)
		return edgesNum;
};

unsigned int TaskGenerator::GeneratePredicatesNum()
{
	auto predicatesNum = _minPredicatesNum + rand() % (_maxPredicatesNum - _minPredicatesNum);
	return predicatesNum;
};

unsigned int TaskGenerator::GenerateRandMarkedStatesNum()
{
	auto statesNum = rand() % _maxStatesNum;
	if (statesNum < _minStatesNum)
		return statesNum;
};