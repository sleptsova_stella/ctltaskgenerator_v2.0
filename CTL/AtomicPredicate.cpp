#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"

AtomicPredicate::AtomicPredicate(std::string name):SubformulaCTL()
{
	_name = name;
	DepthSize = 1;
};

std::set<State*>* AtomicPredicate::SAT(KripkeStructure* kripkeStructure)
{
	auto resultStates = new std::set<State*>();

	for (auto stateAndPredicates : kripkeStructure->_marksFunction)
	{
		auto state = stateAndPredicates.first;
		auto predicates = stateAndPredicates.second;

		if (predicates.find(this) != predicates.end())
			resultStates->insert(state);
	}

	return resultStates;
};

std::string AtomicPredicate::GetName()
{
	return _name;
};

std::string AtomicPredicate::GetFullFormula()
{
	return " " + GetName() + " ";
};

std::string AtomicPredicate::GetSubFormula()
{
	return "\\(f_{" + std::to_string(Number) + "} = " + _name + ",\\) ";
};