#pragma once
#include <exception>

class NonValidStatesNumException : public std::exception
{
public:
	NonValidStatesNumException() {  };
};

class NonValidEdgesNumException : public std::exception
{
public: 
	NonValidEdgesNumException() {  };
};

class NonValidInitialStatesNumException : public std::exception
{
public:
	NonValidInitialStatesNumException() {  };
};

class StateNonExistException : public std::exception
{
public: 
	StateNonExistException() {  };
};

class CanNotOpenTxtFile : public std::exception
{
public:
	CanNotOpenTxtFile() {  };
};

class SubformulaIsEmpty : public std::exception
{
public: 
	std::string Formula;

public:
	SubformulaIsEmpty(std::string formula)
	{ 
		Formula = formula;
	};
};