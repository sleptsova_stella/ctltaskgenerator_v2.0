#include "stdafx.h"
#include "KripkeStructure.h"
#include "Basis_EX_AF_EU.h"
#include "Exceptions.h"

KripkeStructure::KripkeStructure(std::set<State*> states, std::set<State*> initialStates, 
	std::set<SubformulaCTL*> atomicPredicates,
	std::set<std::pair<State*, State*>> totalRatio, std::map<State*, std::set<SubformulaCTL*>> marksFunction)
{
	_states = states;
	_initialStates = initialStates;
	_atomicPredicates = atomicPredicates;
	_marksFunction = marksFunction;
	_totalRatio = totalRatio;
	SetTotalRatio(totalRatio);	
}

KripkeStructure::~KripkeStructure()
{
}

bool KripkeStructure::ModelCheck(SubformulaCTL* sourceFormula)
{
	for (auto i = 1; i < sourceFormula->DepthSize; i++)
	{
		FindSubformula(sourceFormula, i);
	}

	auto finalSat = sourceFormula->SAT(this);
	for (auto s : *finalSat)
		_marksFunction[s].insert(sourceFormula);

	for (auto s : *finalSat)
	{
		for (auto s0 : _initialStates)
			if (s0 == s) return true;
	}
	return false;
}

void KripkeStructure::FindSubformula(SubformulaCTL* formulaSat, int depthSize)
{
	if (formulaSat->DepthSize > depthSize)
	{
		if (formulaSat->_p!=nullptr && !formulaSat->Marked)
			 FindSubformula(formulaSat->_p, depthSize);
		if (formulaSat->_q!=nullptr && !formulaSat->Marked)
			 FindSubformula(formulaSat->_q, depthSize);
		return;
	}

	formulaSat->Marked = true;
	auto sat = formulaSat->SAT(this);
	for (auto s : *sat)
		_marksFunction[s].insert(formulaSat);
}

void KripkeStructure::SetStates(std::set<State*> states)
{
	if (states.empty())
		throw new std::invalid_argument("States is empty");

	const auto statesMinNum = 4;
	if (states.size() < statesMinNum) 
		throw new NonValidStatesNumException();

	_states = states;
};

void KripkeStructure::SetInitialStates(std::set<State*> initialStates)
{
	if (initialStates.empty())
		throw new std::invalid_argument("Initial states is empty");

	const auto initStatesMinNum = 1;
	if (initialStates.size() < initStatesMinNum && initialStates.size() > _states.size())
		throw new NonValidInitialStatesNumException();

	for (auto state : initialStates)
	{
		if (_states.find(state) == _states.end())
			throw new StateNonExistException();
	}

	_initialStates = initialStates;
};

void KripkeStructure::SetMarksFunction(std::map<State*, std::set<SubformulaCTL*>> marksFunction)
{
	if (marksFunction.empty())
		throw new std::invalid_argument("Mark function is empty");

	for (auto stateAndFormula : marksFunction)
	{
		auto state = stateAndFormula.first;
		if (_states.find(state) == _states.end())
			throw new StateNonExistException();
	}

	_marksFunction = marksFunction;
};

void KripkeStructure::SetTotalRatio(std::set<std::pair<State*, State*>> totalRatio)
{
	if (totalRatio.empty())
		throw  new std::invalid_argument("Totoal ratio is empty");

	for (auto ratio : totalRatio)
	{
		if (_states.find(ratio.first) == _states.end())
			throw new StateNonExistException();

		if (_states.find(ratio.second) == _states.end())
			throw new StateNonExistException();
	}

	for (auto state : _states)
	{
		_statesWithChilren[state] = *(new std::set<State*>());
		for (auto ratio : totalRatio)
		{
			if (ratio.first == state)
				_statesWithChilren[state].insert(ratio.second);
		}
	}
};

std::string KripkeStructure::GetStateNames()
{
	std::string stateNames = "";

	for (auto state : _states)
		stateNames = stateNames + state->Name + ",";

	stateNames = stateNames.substr(0, stateNames.size() - 1);
	return stateNames;
};

std::string KripkeStructure::GetInitialStateNames()
{
	std::string stateNames = "";

	for (auto state : _initialStates)
		stateNames += state->Name + ",";

	stateNames = stateNames.substr(0, stateNames.size() - 1);
	return stateNames;
};

std::string KripkeStructure::GetTotalRatio()
{
	std::string totalRatio = "";

	for (auto ratio : _totalRatio)
		totalRatio += "(" + ratio.first->Name + "," + ratio.second->Name + "),";

	totalRatio = totalRatio.substr(0, totalRatio.size() - 1);
	return totalRatio;
};

std::string KripkeStructure::GetAtomicPredicate()
{
	std::string predicates = "";

	for (auto predicate : _atomicPredicates)
	{
		auto atomicaPredicate = (AtomicPredicate*)predicate;
		predicates += atomicaPredicate->GetName() + ",";
	}

	predicates = predicates.substr(0, predicates.size() - 1);
	return predicates;
};

std::string KripkeStructure::GetMarksFunction()
{
	std::string marks = "";

	for (auto state : _states)
	{
		marks += "\\( L(" + state->Name + ")=";
		
		auto markedPredicates = _marksFunction.find(state);
		if (markedPredicates != _marksFunction.end() &&
			(*markedPredicates).second.size() > 0)
		{
			marks += "\\{";
			for (auto predicate : (*markedPredicates).second)
			{
				auto atomicPredicate = (AtomicPredicate*)predicate;
				marks += atomicPredicate->GetName() + ",";
			}
			marks = marks.substr(0, marks.size() - 1);
			marks += "\\}";
		}
		else
			marks += "\\emptyset";
		marks += ",\\)";
	}

	marks = marks.substr(0, marks.size() - 3);
	marks += "\\)";

	return marks;
};

std::string KripkeStructure::GetMarksStates()
{
	std::string marks = "<choicegroup label=\" ����������� �� �� ������ "
		"��������� ������ ��������� ������� CTL \" type=\"MultipleChoice\" "
		"answer-pool=\"5\">";

	for (auto state : _states)
	{
		marks += "<choice correct=\"true\" explanation-id=\" "
			+ std::to_string(rand()) + "  \">";

		auto markedPredicates = _marksFunction.find(state);
		if (markedPredicates != _marksFunction.end() &&
			(*markedPredicates).second.size() > 0)
		{
			marks += "\(" + state->Name + "\) \(\{";
			for (auto predicate : (*markedPredicates).second)
			{
				marks += std::to_string(predicate->Number) + ",";
			}
			marks = marks.substr(0, marks.size() - 1);
		}
		else
			marks += "\\emptyset";
		marks += "\}\)</choice> \n";
	}

	return marks;
};

//need implement Kosaraju's algorithm
