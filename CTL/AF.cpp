#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"
#include "Exceptions.h"

AF::AF(SubformulaCTL* subFormula) : SubformulaCTL()
{
	_p = subFormula;
	DepthSize = _p->DepthSize + 1;
};

std::set<State*>* AF::SAT(KripkeStructure* kripkeStructure)
{
	auto X = kripkeStructure->_states;
	auto Y = new std::set<State*>();
	
	for (auto stateAndPredicates : kripkeStructure->_marksFunction)
	{
		auto state = stateAndPredicates.first;
		auto predicates = stateAndPredicates.second;

		if (predicates.find(_p) != predicates.end())
			Y->insert(state);
	}

	while (!CompareTwoStatesSets(X, *Y))
	{
		X = *Y;

		for (auto parentState : kripkeStructure->_states)
		{
			auto childrenAreMarked = true;
			auto children = kripkeStructure->_statesWithChilren[parentState];
			for (auto child : children)
			{
				if (Y->find(child) == Y->end())
				{
					childrenAreMarked = false;
					break;
				}
			}
			
			if (childrenAreMarked)
				Y->insert(parentState);
		}
	}

	return Y;
};

std::string AF::GetFullFormula()
{
	std::string fullCTLFormula = " \\mathrm{AF} (";
	if (_p != nullptr)
		return fullCTLFormula + _p->GetFullFormula() + ")";
	else
		throw new SubformulaIsEmpty(typeid(AF).name());
};

std::string AF::GetSubFormula()
{
	return "\\(f_{" + std::to_string(Number) + "} = \\mathrm{AF} f_{" +
		std::to_string(_p->Number) + "},\\) " + _p->GetSubFormula();
};