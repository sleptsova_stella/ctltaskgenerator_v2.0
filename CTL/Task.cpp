#include "stdafx.h"
#include "TaskGenerator.h"
#include "KripkeStructure.h"
#include "Basis_EX_AF_EU.h"


Task::Task(unsigned int statesNum, unsigned int edgesNum, unsigned int predicatesNum,	
	unsigned int initialStatesNum, unsigned int markedStatesNum,
	unsigned int maxPredicatesNumInOneState)
{
	_statesNum = statesNum;
	_initialStatesNum = initialStatesNum;
	_edgesNum = edgesNum;
	_predicatesNum = predicatesNum;
	_markedStatesNum = markedStatesNum;

	if (maxPredicatesNumInOneState > _predicatesNum)
		_maxPredicatesNumInOneState = _predicatesNum;
	else
		_maxPredicatesNumInOneState = maxPredicatesNumInOneState;

	GenerateKripkeStructure();
};

KripkeStructure* Task::GetKripkeStructure()
{
	return _kripkeStructure;
};

SubformulaCTL* Task::GetFormulaCTL()
{
	if (_formulaCTL == nullptr)
		_formulaCTL = GenerateFormulaCTL(*_atomicPredicates);
	return _formulaCTL;
}

void Task::GenerateKripkeStructure()
{
	auto states = GenerateStates();
	auto initialStates = GenerateInitialStates(states);
	auto totalRatio = GenerateTotalRatio(states);
	_atomicPredicates = GenerateAtomicPredicates();
	auto marksFunction = GenerateMarksFunction(states,*_atomicPredicates);

	_kripkeStructure = new KripkeStructure(*states, *initialStates,
		*_atomicPredicates, totalRatio, marksFunction);
};

std::set<State*>* Task::GenerateStates()
{
	const auto nameState = "s_";
	auto states = new std::set<State*>();

	for (unsigned int i = 0; i < _statesNum; i++)
	{
		auto state = new State(nameState + std::to_string(i));
		states->insert(state);
	}

	return states;
};

std::set<State*>* Task::GenerateInitialStates(std::set<State*>* states)
{
	auto initialStates = new std::set<State*>();

	for (unsigned int currentInitialStateNum = 0; currentInitialStateNum < _initialStatesNum; currentInitialStateNum++)
	{
		auto stateNum = rand() % (_statesNum - 1);
		auto state = *std::next(states->begin(), stateNum);
		initialStates->insert(state);
	}

	return initialStates;
};

std::set<std::pair<State*, State*>> Task::GenerateTotalRatio(std::set<State*>* states)
{
	auto totalRatio = new std::set<std::pair<State*, State*>>();

	for (unsigned int currentEdgeNum = 0; currentEdgeNum < 14; currentEdgeNum++)
	{
		auto leftStateNum = GenerateRandStateNum();
		auto rightStateNum = GenerateRandStateNum();

		auto leftState = *std::next(states->begin(), leftStateNum);
		auto rightState = *std::next(states->begin(), rightStateNum);

		auto edge = std::make_pair(leftState, rightState);

		auto edgeExists = false;
		for (auto ratio : *totalRatio)
		{
			if (ratio.first == leftState && ratio.second == rightState)
			{
				edgeExists = true;
				break;
			}
		}

		if (edgeExists)
		{
			currentEdgeNum--;
			continue;
		}

		totalRatio->insert(edge);
	}

	//TO DO:: !!need check connectivity!!!!
	return *totalRatio;
};

unsigned int Task::GenerateRandStateNum()
{
	auto randStateNum = rand() % _statesNum;
	if (randStateNum < _statesNum)
		return randStateNum;
	return GenerateRandStateNum();
}

std::map<State*, std::set<SubformulaCTL*>> Task::GenerateMarksFunction(std::set<State*>* states, 
	std::set<SubformulaCTL*> atomicPredicates)
{
	auto marksFunction = new std::map<State*, std::set<SubformulaCTL*>>();

	for (unsigned int stateNum = 0; stateNum < _markedStatesNum; stateNum++)
	{
		auto randStateNum = GenerateRandStateNum();
		auto state = *std::next(states->begin(), randStateNum);

		if (marksFunction->find(state) != marksFunction->end())
		{
			stateNum--;
			continue;
		}

		if (rand() % 5 > 1)
		{
			auto atomicPredicatesCountInTheState = rand() % _maxPredicatesNumInOneState + 1;
			auto atomicPredicatesInTheState = new std::set<SubformulaCTL*>();

			for (unsigned int atomicPredicateNum = 0; atomicPredicateNum < atomicPredicatesCountInTheState; atomicPredicateNum++)
			{
				auto randAtomicPredicateNum = rand() % _predicatesNum;
				auto randAtomicPredicate = *std::next(atomicPredicates.begin(), randAtomicPredicateNum);

				if (atomicPredicatesInTheState->find(randAtomicPredicate) != atomicPredicatesInTheState->end())
				{
					atomicPredicateNum--;
					continue;
				}

				atomicPredicatesInTheState->insert(randAtomicPredicate);
			}

			(*marksFunction)[state] = *atomicPredicatesInTheState;
		}
		else
			(*marksFunction)[state] = std::set<SubformulaCTL*>();
	}

	return *marksFunction;
};

std::set<SubformulaCTL*>* Task::GenerateAtomicPredicates()
{
    auto namesAtomicPredicates = new std::string[7]{ "p", "q","w","r","z","t","g" };

	auto atomicPredicates = new std::set<SubformulaCTL*>();

	for (unsigned int predicateNum = 0; predicateNum < _predicatesNum; predicateNum++)
		atomicPredicates->insert(new AtomicPredicate(namesAtomicPredicates[predicateNum]));

	return atomicPredicates;
};

SubformulaCTL* Task::GenerateFormulaCTL(std::set<SubformulaCTL*> atomicPredicate)
{
	/*auto p = std::next(atomicPredicate.begin(), 0);
	auto q = std::next(atomicPredicate.begin(), 1);
	auto r = std::next(atomicPredicate.begin(), 2);
	auto non_p = new FormulaNegative(*p);
	auto q_or_r = new Or(*q, *r);
	auto ex_non_p = new EX(non_p);
	auto af_q_or_r = new AF(q_or_r);
	auto formula = new EU(ex_non_p, af_q_or_r);*/

	/*auto a = std::next(atomicPredicate.begin(), 0);
	auto b = std::next(atomicPredicate.begin(), 1);
	auto nea = new FormulaNegative(*a);
	auto exb = new EX(*b);
	auto neexb = new FormulaNegative(exb);
	auto or1 = new Or(nea, neexb);
	auto eu = new EU(nea, or1);
	auto neeu = new FormulaNegative(eu);
	auto af = new AF(*a);
	auto formula = new Or(af, neeu);*/

	unsigned int formulaDeep = 4 + rand() % 3;
	unsigned int maxDeep = 1;
	const unsigned int formulasCTLCount = 5;
	auto allFormula = new std::set<SubformulaCTL*>();
	SubformulaCTL* formulaCTL = nullptr;

	for (auto predicate : atomicPredicate)
		allFormula->insert(predicate);

	while (maxDeep < formulaDeep)
	{
		int formulaIndex = rand() % formulasCTLCount;
		SubformulaCTL* formula = nullptr;
		int subFormulaNumP = 0;
		int subFormulaNumQ = 0;
		switch (formulaIndex)
		{
		case 0:
			subFormulaNumP = rand() % allFormula->size();
			formula = new FormulaNegative(*std::next(allFormula->begin(), subFormulaNumP));
			break;
		case 1:
			subFormulaNumP = rand() % allFormula->size();
			formula = new EX(*std::next(allFormula->begin(), subFormulaNumP));
			break;
		case 2:
			subFormulaNumP = rand() % allFormula->size();
			formula = new AF(*std::next(allFormula->begin(), subFormulaNumP));
			break;
		case 3:
			subFormulaNumP = rand() % allFormula->size();
			subFormulaNumQ = rand() % allFormula->size();
			formula = new EU(*std::next(allFormula->begin(), subFormulaNumP),
				*std::next(allFormula->begin(), subFormulaNumQ));
			break;
		case 4:
			subFormulaNumP = rand() % allFormula->size();
			subFormulaNumQ = rand() % allFormula->size();
			formula = new Or(*std::next(allFormula->begin(), subFormulaNumP),
				*std::next(allFormula->begin(), subFormulaNumQ));
			break;
		default:
			throw new std::exception("Formula doesn't exist");
			break;
		}

		if (maxDeep < formula->DepthSize)
			maxDeep = formula->DepthSize;

		allFormula->insert(formula);
	}

	for (auto formula : *allFormula)
	{
		if (formula->DepthSize == formulaDeep)
		{
			formulaCTL = formula;
			break;
		}
	}

	formulaCTL->InitNumberFormula();
	return formulaCTL;
};

void Task::CheckStronglyConnectedGraph(std::set<std::pair<State*, State*>> totalRatio, std::set<State*> states)
{
	for (auto state : states)
	{
		auto outgoingArcExists = false;
		for (auto edge : totalRatio)
		{
			if (edge.first == state)
			{
				outgoingArcExists = true;
				break;
			}
		}

		if (!outgoingArcExists)
		{
			auto rightStateNum = rand() % _statesNum - 1;
			auto rightState = *std::next(states.begin(), rightStateNum);

			auto edge = std::make_pair(state, rightState);
			totalRatio.insert(edge);
		}
	}


	// check strongly connected 
	auto statesWithChilren = *(new std::map<State*, std::set<State*>>());
	for (auto state : states)
	{
		statesWithChilren[state] = *(new std::set<State*>());
		for (auto ratio : totalRatio)
		{
			if (ratio.first == state)
				statesWithChilren[state].insert(ratio.second);
		}
	}

	for (auto leftState : states)
	{
		for (auto rightState : states)
		{
			if (leftState == rightState)
				break;

			auto lastIntermediateState = rightState;

		}
	}
};