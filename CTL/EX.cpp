#include "stdafx.h"
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"
#include "Exceptions.h"

EX::EX(SubformulaCTL* subFormula) :SubformulaCTL()
{
	_p = subFormula;
	DepthSize = _p->DepthSize + 1;
};

std::set<State*>* EX::SAT(KripkeStructure* kripkeStructure)
{
	auto resultStates = new std::set<State*>();

	for (auto statesWithChildren : kripkeStructure->_statesWithChilren)
	{
		auto parentState = statesWithChildren.first;
		auto childrenStates = statesWithChildren.second;

		// or may use "go to"
		auto childrenContainPredicate = false;
		for (auto children : childrenStates)
		{
			for (auto predicate : kripkeStructure->_marksFunction[children])
			{
				if (predicate == _p)
				{
					childrenContainPredicate = true;
					resultStates->insert(parentState);
					break;
				}
			}

			// or may use "go to"
			if (childrenContainPredicate)
				break;
		}
	}

	return resultStates;
};

std::string EX::GetFullFormula()
{
	std::string fullCTLFormula = "\\mathrm{EX} (";
	if (_p != nullptr)
		return fullCTLFormula + _p->GetFullFormula() + ")";
	else
		throw new SubformulaIsEmpty(typeid(EX).name());
};

std::string EX::GetSubFormula()
{
	return " \\(f_{" + std::to_string(Number) +
		"} = \\mathrm{EX} f_{" + std::to_string(_p->Number) + "},\\) "
		+ _p->GetSubFormula();
};