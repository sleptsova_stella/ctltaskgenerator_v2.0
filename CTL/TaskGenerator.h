#pragma once
#include "Basis_EX_AF_EU.h"
#include "KripkeStructure.h"

#ifndef H_Task
#define H_Task
class Task
{
private:
	unsigned int _statesNum;
	unsigned int _initialStatesNum;
	unsigned int _edgesNum;
	unsigned int _predicatesNum;
	unsigned int _markedStatesNum;
	unsigned int _maxPredicatesNumInOneState;
	SubformulaCTL* _formulaCTL;
	KripkeStructure* _kripkeStructure;
	std::set<SubformulaCTL*>* _atomicPredicates;
public:
	Task(unsigned int statesNum, unsigned int edgesNum,	unsigned int predicatesNum, 
		unsigned int initialStatesNum, unsigned int markedStatesNum,
		unsigned int maxPredicatesNumInOneState);
	KripkeStructure* GetKripkeStructure();
	SubformulaCTL* GetFormulaCTL();
private:
	void GenerateKripkeStructure();
	std::set<State*>* GenerateStates();
	std::set<State*>* GenerateInitialStates(std::set<State*>* states);
	std::set<std::pair<State*, State*>> GenerateTotalRatio(std::set<State*>* state);
	std::map<State*, std::set<SubformulaCTL*>> GenerateMarksFunction(std::set<State*>* state,
		std::set<SubformulaCTL*> atomicPredicates);
	std::set<SubformulaCTL*>* GenerateAtomicPredicates();

	SubformulaCTL* GenerateFormulaCTL(std::set<SubformulaCTL*> atomicPredicates);

	void CheckStronglyConnectedGraph(std::set<std::pair<State*, State*>> totalRatio, std::set<State*> states);
	unsigned int GenerateRandStateNum();
};
#endif


#ifndef H_TaskGenerator
#define H_TaskGenerator
class __declspec(dllexport) TaskGenerator
{
private:
	const int _minStatesNum = 4;
	const int _minInitialStatesNum = 1;
	const int _minEdgesNum = 9;
	const int _minPredicatesNum = 2;

	unsigned int _tasksNum;
	unsigned int _maxStatesNum;
	unsigned int _maxInitialStatesNum;
	unsigned int _maxEdgesNum;
	unsigned int _maxPredicatesNum;

	/**
		Max states nuumber which must be marked by atomic predicates
	*/
	unsigned int _maxMarkedStatesNum;
public:
	TaskGenerator(unsigned int tasksNum, unsigned int maxStatesNum,
		unsigned int maxEdgesNum, unsigned int maxPredicatesNum, unsigned int maxInitialStatesNum,
		unsigned int maxMarkedStatesNum);
	void Generate();

private:
	unsigned int GenerateStatesNum();
	unsigned int GenerateInitailStatesNum();
	unsigned int GenerateEdgesNum();
	unsigned int GeneratePredicatesNum();
	/**
	Generate states number which must be marked by atomic predicates
	*/
	unsigned int GenerateRandMarkedStatesNum();
};
#endif