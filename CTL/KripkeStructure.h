#pragma once
#include "Basis_EX_AF_EU.h"
#include <map>

#ifndef H_KripkeStructure
#define H_KripkeStructure

class __declspec(dllexport) KripkeStructure
{
private:
	std::map<State*, std::set<State*>> _statesWithChilren;
	std::set<State*> _states;
	std::set<State*> _initialStates;
	// may be use tree because need check children states in EX
	std::set<std::pair<State*, State*>> _totalRatio;
	std::set<SubformulaCTL*> _atomicPredicates;
	std::map<State*, std::set<SubformulaCTL*>> _marksFunction;

public:
	KripkeStructure(std::set<State*> states, std::set<State*> initStates,
		std::set<SubformulaCTL*> atomicPredicates,
		std::set<std::pair<State*, State*>> totalRatio,
		std::map<State*, std::set<SubformulaCTL*>> marksFunction);
	~KripkeStructure();

	bool ModelCheck(SubformulaCTL* sourceFormula);
	
	void SetStates(std::set<State*> states);
	void SetInitialStates(std::set<State*> initialStates);
	void SetMarksFunction(std::map<State*, std::set<SubformulaCTL*>> marksFunction);
	void SetTotalRatio(std::set<std::pair<State*, State*>> totalRatio);
	
	std::string GetStateNames();
	std::string GetInitialStateNames();
	std::string GetTotalRatio();
	std::string GetAtomicPredicate();
	std::string GetMarksFunction();
	std::string GetMarksStates();

	//need to check atomicsPrdicates and marskfunction

private:
	KripkeStructure();
	void FindSubformula(SubformulaCTL* formulaSat, int DepthSize);
	

	friend class SubformulaCTL;
	friend class AtomicPredicate;
	friend class FormulaNegative;
	friend class Or;
	friend class EX;
	friend class AF;
	friend class EU;
};
#endif