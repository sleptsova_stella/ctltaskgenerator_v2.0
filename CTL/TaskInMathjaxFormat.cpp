#include "stdafx.h"
#include "TaskInMathjaxFormat .h"
#include <iostream>
#include <fstream>
#include "KripkeStructure.h"
#include "TaskGenerator.h"
#include "Exceptions.h"

TaskInMathjaxFormat::TaskInMathjaxFormat(Task* task)
{
	_task = task;
	GenerateTaskInMathjaxFormat();
};

void TaskInMathjaxFormat::GenerateTaskInMathjaxFormat()
{
	std::ofstream sourceFile;
	sourceFile.open(_sourceFileName, std::ios::app);

	if (!sourceFile.good())
	{
		throw new CanNotOpenTxtFile();
	}
	
	auto kripkeStructure = _task->GetKripkeStructure();
	sourceFile << _header;
	sourceFile << "<li> \\( S = \{\ " + kripkeStructure->GetStateNames() + "\\} \\)</li> \n";
	sourceFile << "<li> \\(S_0 = \\{  " + kripkeStructure->GetInitialStateNames() + " \\} \\)</li>\n";
	sourceFile << "<li> \\( R = \\{ " + kripkeStructure->GetTotalRatio() + " \\} \\)</li> \n";
	sourceFile << "<li> \\(AP = \\{ " + kripkeStructure->GetAtomicPredicate() + "\\} \\)</li> \n";
	sourceFile << "<li> " + kripkeStructure->GetMarksFunction() + " </li> \n";
	sourceFile << "</ul>\n";

	auto formulaCTL = _task->GetFormulaCTL();
	sourceFile << _startWording;
	sourceFile << formulaCTL->GetFullFormula() + ".\\)";
	sourceFile << _endWorking;

	sourceFile << _subformulaDesignation;
	sourceFile << formulaCTL->GetSubFormula();
	sourceFile << _taskEnd;
	sourceFile << "\n \n";

	kripkeStructure->ModelCheck(formulaCTL);
	sourceFile << "<multiplechoiceresponse> \n";
	sourceFile << kripkeStructure->GetMarksStates();
	sourceFile << " </choicegroup> \n </multiplechoiceresponse> \n";

	sourceFile << "\n \n";
	sourceFile.close();
};