#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\CTL\Basis_EX_AF_EU.h"
#include "..\CTL\KripkeStructure.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CTL_Tests
{
	TEST_CLASS(CTL_Tests)
	{
	private:
		KripkeStructure * kripkeStructureTest;
		State* s0;
		State* s1;
		State* s2;
		State* s3;
		State* s4;
		State* s5;
		AtomicPredicate* p;
		AtomicPredicate* q;
		AtomicPredicate* r;

		void InitKripkeStructureTest()
		{
			s0 = new State("s0");
			s1 = new State("s1");
			s2 = new State("s2");
			s3 = new State("s3");
			s4 = new State("s4");
			s5 = new State("s5");
			p = new AtomicPredicate("p");
			q = new AtomicPredicate("q");
			r = new AtomicPredicate("r");
			auto atomicPredocates = new std::set<SubformulaCTL*>{ p, q, r };

			auto states = std::set<State*>{ s0, s1, s2, s3, s4, s5 };
			auto initialStates = std::set<State*>{ s0 };
			auto totalRatio = std::set<std::pair<State*, State*>>();
			totalRatio.insert(std::make_pair(s0, s1));
			totalRatio.insert(std::make_pair(s0, s3));
			totalRatio.insert(std::make_pair(s1, s4));
			totalRatio.insert(std::make_pair(s4, s0));
			totalRatio.insert(std::make_pair(s4, s3));
			totalRatio.insert(std::make_pair(s4, s5));
			totalRatio.insert(std::make_pair(s3, s0));
			totalRatio.insert(std::make_pair(s3, s5));
			totalRatio.insert(std::make_pair(s5, s2));
			totalRatio.insert(std::make_pair(s2, s1));
			totalRatio.insert(std::make_pair(s2, s3));
			auto marksFunction = *(new std::map<State*, std::set<SubformulaCTL*>>());
			marksFunction[s0] = std::set<SubformulaCTL*>();
			marksFunction[s1] = std::set<SubformulaCTL*>{ p };
			marksFunction[s2] = std::set<SubformulaCTL*>{ p };
			marksFunction[s3] = std::set<SubformulaCTL*>{ p };
			marksFunction[s4] = std::set<SubformulaCTL*>{ p,q };
			marksFunction[s5] = std::set<SubformulaCTL*>{ p,r };

			kripkeStructureTest = new KripkeStructure(states, initialStates,
				*atomicPredocates, totalRatio, marksFunction);
		}

		void AssertSetContain(std::set<State*>* set, State *state, bool isContain)
		{
			Assert::AreEqual(set->find(state) != set->end(), isContain);
		}

	public:

		TEST_METHOD(AtomicPredicate_Sat_Test)
		{
			InitKripkeStructureTest();
			auto states = p->SAT(kripkeStructureTest);

			AssertSetContain(states, s0, false);
			AssertSetContain(states, s1, true);
			AssertSetContain(states, s2, true);
			AssertSetContain(states, s3, true);
			AssertSetContain(states, s4, true);
			AssertSetContain(states, s5, true);
		};

		TEST_METHOD(AtomicPredicateNegative_Sat_Test)
		{
			InitKripkeStructureTest();
			auto nonPredicate = new FormulaNegative(p);
			auto states = nonPredicate->SAT(kripkeStructureTest);

			AssertSetContain(states, s0, true);
			AssertSetContain(states, s1, false);
			AssertSetContain(states, s2, false);
			AssertSetContain(states, s3, false);
			AssertSetContain(states, s4, false);
			AssertSetContain(states, s5, false);
		};

		TEST_METHOD(OrAtomicPredicates_Sat_Test)
		{
			InitKripkeStructureTest();
			auto orPredicates = new Or(p, q);
			auto states = orPredicates->SAT(kripkeStructureTest);

			AssertSetContain(states, s0, false);
			AssertSetContain(states, s1, true);
			AssertSetContain(states, s2, true);
			AssertSetContain(states, s3, true);
			AssertSetContain(states, s4, true);
			AssertSetContain(states, s5, true);
		};

		TEST_METHOD(EX_Sat_Test)
		{
			InitKripkeStructureTest();
			auto ex_p = new EX(p);
			auto ex_q = new EX(q);
			auto ex_r = new EX(r);
			auto states_p = ex_p->SAT(kripkeStructureTest);
			auto states_q = ex_q->SAT(kripkeStructureTest);
			auto states_r = ex_r->SAT(kripkeStructureTest);

			AssertSetContain(states_p, s0, true);
			AssertSetContain(states_p, s1, true);
			AssertSetContain(states_p, s2, true);
			AssertSetContain(states_p, s3, true);
			AssertSetContain(states_p, s4, true);
			AssertSetContain(states_p, s5, true);

			AssertSetContain(states_q, s0, false);
			AssertSetContain(states_q, s1, true);
			AssertSetContain(states_q, s2, false);
			AssertSetContain(states_q, s3, false);
			AssertSetContain(states_q, s4, false);
			AssertSetContain(states_q, s5, false);

			AssertSetContain(states_r, s0, false);
			AssertSetContain(states_r, s1, false);
			AssertSetContain(states_r, s2, false);
			AssertSetContain(states_r, s3, true);
			AssertSetContain(states_r, s4, true);
			AssertSetContain(states_r, s5, false);
		};

		TEST_METHOD(AF_Sat_Test)
		{
			InitKripkeStructureTest();
			auto af_p = new AF(p);
			auto af_q = new AF(q);
			auto af_r = new AF(r);
			auto states_p = af_p->SAT(kripkeStructureTest);
			auto states_q = af_q->SAT(kripkeStructureTest);
			auto states_r = af_r->SAT(kripkeStructureTest);

			AssertSetContain(states_p, s0, true);
			AssertSetContain(states_p, s1, true);
			AssertSetContain(states_p, s2, true);
			AssertSetContain(states_p, s3, true);
			AssertSetContain(states_p, s4, true);
			AssertSetContain(states_p, s5, true);

			AssertSetContain(states_q, s0, false);
			AssertSetContain(states_q, s1, true);
			AssertSetContain(states_q, s2, false);
			AssertSetContain(states_q, s3, false);
			AssertSetContain(states_q, s4, true);
			AssertSetContain(states_q, s5, false);

			AssertSetContain(states_r, s0, false);
			AssertSetContain(states_r, s1, false);
			AssertSetContain(states_r, s2, false);
			AssertSetContain(states_r, s3, false);
			AssertSetContain(states_r, s4, false);
			AssertSetContain(states_r, s5, true);
		};

		TEST_METHOD(EU_Sat_Test)
		{
			InitKripkeStructureTest();
			auto epuq = new EU(p, q);
			auto equp = new EU(q, p);
			auto epur = new EU(p, r);
			auto erup = new EU(r, p);
			auto equr = new EU(q, r);
			auto eruq = new EU(r, q);
			auto states_pq = epuq->SAT(kripkeStructureTest);
			auto states_qp = equp->SAT(kripkeStructureTest);
			auto states_pr = epur->SAT(kripkeStructureTest);
			auto states_rp = erup->SAT(kripkeStructureTest);
			auto states_qr = equr->SAT(kripkeStructureTest);
			auto states_rq = eruq->SAT(kripkeStructureTest);

			AssertSetContain(states_pq, s0, false);
			AssertSetContain(states_pq, s1, true);
			AssertSetContain(states_pq, s2, true);
			AssertSetContain(states_pq, s3, true);
			AssertSetContain(states_pq, s4, true);
			AssertSetContain(states_pq, s5, true);

			AssertSetContain(states_qp, s0, false);
			AssertSetContain(states_qp, s1, true);
			AssertSetContain(states_qp, s2, true);
			AssertSetContain(states_qp, s3, true);
			AssertSetContain(states_qp, s4, true);
			AssertSetContain(states_qp, s5, true);

			AssertSetContain(states_pr, s0, false);
			AssertSetContain(states_pr, s1, true);
			AssertSetContain(states_pr, s2, true);
			AssertSetContain(states_pr, s3, true);
			AssertSetContain(states_pr, s4, true);
			AssertSetContain(states_pr, s5, true);

			AssertSetContain(states_rp, s0, false);
			AssertSetContain(states_rp, s1, true);
			AssertSetContain(states_rp, s2, true);
			AssertSetContain(states_rp, s3, true);
			AssertSetContain(states_rp, s4, true);
			AssertSetContain(states_rp, s5, true);

			AssertSetContain(states_qr, s0, false);
			AssertSetContain(states_qr, s1, false);
			AssertSetContain(states_qr, s2, false);
			AssertSetContain(states_qr, s3, false);
			AssertSetContain(states_qr, s4, true);
			AssertSetContain(states_qr, s5, true);

			AssertSetContain(states_rq, s0, false);
			AssertSetContain(states_rq, s1, false);
			AssertSetContain(states_rq, s2, false);
			AssertSetContain(states_rq, s3, false);
			AssertSetContain(states_rq, s4, true);
			AssertSetContain(states_rq, s5, false);
		};

		TEST_METHOD(KripkeStructure_Sat)
		{
			InitKripkeStructureTest();
			auto non_p = new FormulaNegative(p);
			auto q_or_r = new Or(q, r);
			auto ex_non_p = new EX(non_p);
			auto af_q_or_r = new AF(q_or_r);
			auto formula = new EU(ex_non_p, af_q_or_r);
			auto resultSat = kripkeStructureTest->ModelCheck(formula);

			formula->InitNumberFormula();
			//auto dfgkjdng = formula->GetSubFormula();
			//auto ssdf = kripkeStructureTest->GetMarksStates();
			Assert::AreEqual(false, resultSat);
		};


		TEST_METHOD(KripkeStructure_Sat1)
		{
			auto s0 = new State("s0");
			auto s1 = new State("s1");
			auto s2 = new State("s2");
			auto s3 = new State("s3");
			auto s4 = new State("s4");
			auto s5 = new State("s5");
			auto w = new AtomicPredicate("w");
			auto q = new AtomicPredicate("q");
			auto p = new AtomicPredicate("p");
			auto atomicPredocates = new std::set<SubformulaCTL*>{ p, q, r };

			auto states = std::set<State*>{ s0, s1, s2, s3, s4, s5 };
			auto initialStates = std::set<State*>{ s0 };
			auto totalRatio = std::set<std::pair<State*, State*>>();
			totalRatio.insert(std::make_pair(s4, s5));
			totalRatio.insert(std::make_pair(s4, s0));
			totalRatio.insert(std::make_pair(s5, s0));
			totalRatio.insert(std::make_pair(s5, s1));
			totalRatio.insert(std::make_pair(s5, s3));
			totalRatio.insert(std::make_pair(s0, s0));
			totalRatio.insert(std::make_pair(s0, s2));
			totalRatio.insert(std::make_pair(s0, s1));
			totalRatio.insert(std::make_pair(s2, s2));
			totalRatio.insert(std::make_pair(s2, s3));
			totalRatio.insert(std::make_pair(s1, s4));
			totalRatio.insert(std::make_pair(s3, s4));
			totalRatio.insert(std::make_pair(s3, s5));
			auto marksFunction = *(new std::map<State*, std::set<SubformulaCTL*>>());
			marksFunction[s0] = std::set<SubformulaCTL*>{ w, q, p};
			marksFunction[s1] = std::set<SubformulaCTL*>();
			marksFunction[s2] = std::set<SubformulaCTL*>();
			marksFunction[s3] = std::set<SubformulaCTL*>{w,q};
			marksFunction[s4] = std::set<SubformulaCTL*>{ w,q};
			marksFunction[s5] = std::set<SubformulaCTL*>();

			auto kripkeStructureTest = new KripkeStructure(states, initialStates,
				*atomicPredocates, totalRatio, marksFunction);
			auto porq = new Or(p, q);
			auto eu3 = new EU(p, q);
			auto eu5 = new EU(eu3, w);
			auto eu6 = new EU(eu3, eu5);
			auto af = new AF(eu6);
			auto f = new FormulaNegative(af);

			auto resultSat = kripkeStructureTest->ModelCheck(f);

			f->InitNumberFormula();
			auto dfgkjdng = f->GetSubFormula();
			auto ssdf = kripkeStructureTest->GetMarksStates();
			Assert::AreEqual(false, resultSat);
		};
	};
}