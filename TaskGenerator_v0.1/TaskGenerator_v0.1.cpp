// TaskGenerator_v0.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "..\CTL\Basis_EX_AF_EU.h"
#include "..\CTL\KripkeStructure.h"
#include "..\CTL\TaskGenerator.h"


void AssertSetContain(std::set<State*>* set, State *state)
{
	if (set->find(state) != set->end())
		std::cout << "yes";
}

int main()
{

	/*KripkeStructure * kripkeStructureTest = new KripkeStructure();

		auto s0 = new State("s0");
		auto s1 = new State("s1");
		auto s2 = new State("s2");
		auto s3 = new State("s3");
		auto s4 = new State("s4");
		auto s5 = new State("s5");

		kripkeStructureTest->States.insert(s0);
		kripkeStructureTest->States.insert(s1);
		kripkeStructureTest->States.insert(s2);
		kripkeStructureTest->States.insert(s3);
		kripkeStructureTest->States.insert(s4);
		kripkeStructureTest->States.insert(s5);

		kripkeStructureTest->InitialStates.insert(s0);

		kripkeStructureTest->TotalRatio.insert(std::make_pair(s0, s1));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s0, s3));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s1, s4));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s4, s0));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s4, s3));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s4, s5));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s3, s0));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s3, s5));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s5, s2));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s2, s1));
		kripkeStructureTest->TotalRatio.insert(std::make_pair(s2, s3));

		auto p = new AtomicPredicate("p");
		auto q = new AtomicPredicate("q");
		auto r = new AtomicPredicate("r");

		kripkeStructureTest->AtomicPredicates.insert(p);
		kripkeStructureTest->AtomicPredicates.insert(q);
		kripkeStructureTest->AtomicPredicates.insert(r);

		kripkeStructureTest->MarksFunction[s0] = std::set<SubformulaCTL*>();
		kripkeStructureTest->MarksFunction[s1] = std::set<SubformulaCTL*>{ p };
		kripkeStructureTest->MarksFunction[s2] = std::set<SubformulaCTL*>{ p };
		kripkeStructureTest->MarksFunction[s3] = std::set<SubformulaCTL*>{ p };
		kripkeStructureTest->MarksFunction[s4] = std::set<SubformulaCTL*>{ p,q };
		kripkeStructureTest->MarksFunction[s5] = std::set<SubformulaCTL*>{ p,r };



		auto states = p->SAT(kripkeStructureTest);

		std::cout << states->size() << "\n";

		AssertSetContain(states, s1);
		AssertSetContain(states, s2);
		AssertSetContain(states, s3);
		AssertSetContain(states, s4);
		AssertSetContain(states, s5);
		AssertSetContain(states, s0);
		*/

	auto taskGenerator = new TaskGenerator(10, 7, 15, 3, 2,3);
	taskGenerator->Generate();

	system("pause");
	return 0;
}